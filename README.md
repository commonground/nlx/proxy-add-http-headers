# proxy-add-http-headers

This repository contains an example on how to add HTTP headers to a request using a proxy. This is useful for NLX users who cannot modify their client to add headers.

Build image:

```shell
docker build -t my-proxy .
```

Replace the values and run the image:

`<outway-address>` replace with the address of your Outway.

`headers-to-add` replace with the HTTP headers you want to add to the request. The format of a header: `<header-name> <header-value>`, if you want to add multiple headers separate the headers by using a comma.

```shell
docker run \
  -e PROXY_PASS=<outway-address> \
  -e HEADERS=<headers-to-add> \
  -p 8080:8080 \
  my-proxy
```

Example values as reference:

```shell
PROXY_PASS=https://nlx-outway-order-vgs-{{DOMAIN_SUFFIX}}/12345678901234567891/daraa/
HEADERS=X-NLX-Request-Delegator 12345678901234567890,X-NLX-Request-Order-Reference order-reference-1
```
