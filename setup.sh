#!/bin/bash
if [[ -z "${PROXY_PASS}" ]]; then
  echo "PROXY_PASS env is required"
  exit 1
fi

sed -i -e "s/{{ PROXY_PASS }}/${PROXY_PASS//\//\\/}/g" /etc/nginx/conf.d/default.conf

IFS=',' read -ra headers_array <<< $HEADERS

headersToAdd=""
for i in "${headers_array[@]}"
do
    headersToAdd+="\tproxy_set_header $i;\n"
done

sed -i -e "s/{{ HEADERS }}/${headersToAdd//\//\\/}/g" /etc/nginx/conf.d/default.conf


cat etc/nginx/conf.d/default.conf

echo "SETUP OK"
exit 0
